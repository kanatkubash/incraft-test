## Usage

 - Install dependencies using `composer install`

### File Seekable Iterator
 - Run test `./vendor/bin/phpunit tests/FileIteratorTest.php` 
 
### Mail Domain Query
 - Create env file `cp .env.example .env`
 - Change env variables to suit your needs
 - Run test `./vendor/bin/phpunit tests/MailQueryTest.php`
 - Keep in mind that test will populate `DBName.users` and `DBName
 .user_email_domains` and truncate on exit. To avoid data loss create test 
 database 
 - As triggers are involved during insert/delete you need to make sure that 
 provided db user has trigger privileges 

