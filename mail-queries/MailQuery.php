<?php
/**
 * Created by PhpStorm.
 * User: kanatkubash
 * Date: 15.04.2019
 * Time: 10:04
 */

namespace MailQuery;

use PDO;

class MailQuery {
	/**
	 * @var PDO
	 */
	protected $client;

	public function __construct(PDO $client) {
		$this->client = $client;
	}

	/** Just gets trigger result
	 * @return array
	 */
	public function getDomainUserCount() {
		$results = $this->client
			->query("SELECT domain,userCount FROM user_email_domains")
			->fetchAll(PDO::FETCH_ASSOC);

		return collect($results)
			->keyBy('domain')
			->map(function(array $values) {
				return intval($values['userCount']);
			})
			->all();
	}

	/** Does processing on runtime
	 * @return array
	 */
	public function getDomainUserCountOnRuntime() {
		$result = $this->client->query("SELECT email from users");

		$domains = [];

		while ($row = $result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
			[$email] = $row;
			$emails = explode(',', $email);
			foreach ($emails as $email) {
				$trimmed = trim($email);
				if ($trimmed == '') {
					if (isset($domains[''])) $domains['']++;
					else $domains[''] = 1;
					continue;
				}

				$domain = explode('@', $trimmed)[1];

				if (isset($domains[$domain]))
					$domains[$domain]++;
				else
					$domains[$domain] = 1;
			}
		}

		return $domains;
	}
}