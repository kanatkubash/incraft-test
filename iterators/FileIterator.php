<?php
/**
 * Created by PhpStorm.
 * User: kanatkubash
 * Date: 15.04.2019
 * Time: 8:45
 */

namespace Iterators;

use ErrorException;

class FileIterator implements \SeekableIterator {
	/**
	 * @var resource
	 */
	protected $handle;
	/**
	 * @var int
	 */
	protected $position;
	/**
	 * @var int
	 */
	protected $fileSize;
	/**
	 * @var string
	 */
	protected $current;
	/**
	 * @var bool
	 */
	protected $isValidPosition;

	public function __construct(string $file) {
		$this->position = 0;

		if (!file_exists($file))
			throw new ErrorException("File not found");
		$this->handle = fopen($file, 'rb');

		$lastError = null;
		set_error_handler(function($errno, $errStr) {
			$lastError = $errStr;

			return true;
		});
		try {
			if ($this->handle === false)
				throw  new ErrorException("Could not open file. $lastError");
		} finally {
			restore_error_handler();
		}

		$this->fileSize = filesize($file);

		if ($this->fileSize == 0) {
			throw new ErrorException("Empty file");
		}

		$this->isValidPosition = true;
	}

	public function __destruct() {
		if ($this->handle)
			fclose($this->handle);
	}

	/**
	 * Return the current element
	 * @link http://php.net/manual/en/iterator.current.php
	 * @return mixed Can return any type.
	 * @since 5.0.0
	 */
	public function current() {
		return $this->current;
	}

	/**
	 * Move forward to next element
	 * @link http://php.net/manual/en/iterator.next.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function next() {
		if ($this->position != $this->fileSize) {
			$this->current = fread($this->handle, 1);
			$this->position++;
		} else {
			$this->isValidPosition = false;
		}
	}

	/**
	 * Return the key of the current element
	 * @link http://php.net/manual/en/iterator.key.php
	 * @return mixed scalar on success, or null on failure.
	 * @since 5.0.0
	 */
	public function key() {
		return $this->position;
	}

	/**
	 * Checks if current position is valid
	 * @link http://php.net/manual/en/iterator.valid.php
	 * @return boolean The return value will be casted to boolean and then evaluated.
	 * Returns true on success or false on failure.
	 * @since 5.0.0
	 */
	public function valid() {
		return $this->isValidPosition;
	}

	/**
	 * Rewind the Iterator to the first element
	 * @link http://php.net/manual/en/iterator.rewind.php
	 * @return void Any returned value is ignored.
	 * @since 5.0.0
	 */
	public function rewind() {
		$this->seek(0);
	}

	/**
	 * Seeks to a position
	 * @link http://php.net/manual/en/seekableiterator.seek.php
	 * @param int $position <p>
	 * The position to seek to.
	 * </p>
	 * @return void
	 * @since 5.1.0
	 */
	public function seek($position) {
		if ($position <= $this->fileSize) {
			$this->position = $position;
			$this->isValidPosition = true;
			fseek($this->handle, $position);
			$this->current = fread($this->handle, 1);
		} else {
			$this->isValidPosition = false;
		}
		echo "\n";
	}

	public function getSize() {
		return $this->fileSize;
	}
}