<?php
/**
 * Created by PhpStorm.
 * User: kanatkubash
 * Date: 15.04.2019
 * Time: 10:25
 */

namespace Tests;

use Dotenv\Dotenv;
use MailQuery\MailQuery;
use PDO;
use PHPUnit\Framework\TestCase;
use Faker\Factory;
use Tightenco\Collect\Support\Collection;

class MailQueryTest extends TestCase {
	/**
	 * @var Collection
	 */
	private $users;
	/**
	 * @var PDO
	 */
	private $client;

	protected function setUp(): void {
		$dotenv = Dotenv::create(__DIR__ . '/../');
		$dotenv->load();
		$this->migrate();
		$this->truncateTables();
		$this->createUsers();
	}

	protected function tearDown(): void {
		$this->truncateTables();
	}

	public function testQueryWorksCorrectly() {
		$query = new MailQuery($this->getClient());
		$result = $query->getDomainUserCount();

		$expected = $this->getExpectedResult();

		ksort($result);
		ksort($expected);

		self::assertSame($expected, $result);
	}

	public function testRuntimeMethodWorksCorrectly() {
		$query = new MailQuery($this->getClient());
		$result = $query->getDomainUserCountOnRuntime();

		$expected = $this->getExpectedResult();

		ksort($result);
		ksort($expected);

		self::assertSame($expected, $result);
	}

	private function getClient(): PDO {
		if ($this->client) return $this->client;

		$dsn = "mysql:host=127.0.0.1;dbname={$_ENV['MYSQL_DB']}";

		return $this->client = new PDO($dsn, $_ENV['MYSQL_USER'], $_ENV['MYSQL_PASS']);
	}

	private function getExpectedResult() {
		return $this->users->reduce(
			function(array $acc, array $user) {
				$emails = explode(',', $user['email']);
				foreach ($emails as $email) {
					$trimmed = trim($email);
					if ($trimmed == '') {
						if (isset($acc[''])) $acc['']++;
						else $acc[''] = 1;
						continue;
					}

					$domain = explode('@', $trimmed)[1];

					if (isset($acc[$domain]))
						$acc[$domain]++;
					else
						$acc[$domain] = 1;
				}

				return $acc;
			},
			[]
		);
	}

	private function createUsers() {

		$factory = Factory::create();
		$this->users = collect([]);

		dump("Creating 10000 users");

		for ($i = 0; $i < 10000; $i++) {
			$emailCount = random_int(0, 4);
			$this->users->push(
				[
					'name'   => $factory->name,
					'gender' => 1,
					'email'  => $emailCount == 0
						? ''
						: collect(range(1, $emailCount))
							->map(function() use ($factory) { return $factory->email; })
							->join(',')
				]
			);
		}

		dump("Inserting 10000 users");

		$this
			->users
			->split(100)
			->each(function(Collection $group) {
				$this->client->beginTransaction();
				$insertValues = $group->map(function(array $item) {
					$name = str_replace('\'', '\\\'', $item['name']);

					return "('{$name}',{$item['gender']},'{$item['email']}')";
				})->join(',');
				/// using exec because we're certain of data clearance
				$this
					->client
					->exec("insert into users(name,gender,email) values $insertValues");
				$this->client->commit();
			});

		dump("Done inserting users");
	}

	private function migrate() {
		$client = $this->getClient();

		$tables = $client->query('show tables')->fetchAll(PDO::FETCH_ASSOC);
		$tablesExist =
			collect($tables)
				->pluck('Tables_in_test')
				->filter(function($tableName) {
					return ($tableName == 'user_email_domains' || $tableName == 'users');
				})
				->count() == 2;

		if (!$tablesExist) {
			$client->exec(file_get_contents(__DIR__ . '/sqls/01-users.sql'));
			$client->exec(file_get_contents(__DIR__ . '/sqls/02-user_email_domains.sql'));
			$client->exec(file_get_contents(__DIR__ . '/sqls/03-insert_trigger.sql'));
			$client->exec(file_get_contents(__DIR__ . '/sqls/04-delete_trigger.sql'));
		}
	}

	private function truncateTables() {
		dump("Truncating 10000 users and user email domains");
		$client = $this->getClient();
		$client->exec('truncate users');
		$client->exec('truncate user_email_domains');
	}
}