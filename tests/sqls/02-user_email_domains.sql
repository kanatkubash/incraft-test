CREATE TABLE `user_email_domains` (
  `domain` varchar(100) NOT NULL,
  `userCount` int(11) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=InnoDB;