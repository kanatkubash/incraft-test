CREATE TRIGGER `users_after_delete` AFTER DELETE ON `users` FOR EACH ROW BEGIN

declare domainCount int default 0;
declare cur int default 0;
declare domain varchar(100);
set domainCount=char_length(OLD.email)-char_length(replace(OLD.email,',',''));

if (domainCount is null) then
set domainCount=0;
else set domainCount=domainCount+1;
end if;

while cur < domainCount do
set domain=trim(substring_index(substring_index(OLD.email,',',cur+1),',',-1));
set domain=substr(domain,locate('@',domain)+1);

insert into user_email_domains (domain,userCount) values (domain,0)
on duplicate key update
userCount=userCount-1;

set cur=cur+1;
end while;

END;
