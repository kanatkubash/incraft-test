<?php
/**
 * Created by PhpStorm.
 * User: kanatkubash
 * Date: 15.04.2019
 * Time: 9:19
 */

namespace Tests;

use Iterators\FileIterator;
use PHPUnit\Framework\TestCase;

class FileIteratorTest extends TestCase {
	const TEXT_FILE = __DIR__ . '/files/testfile';
	const BIN_FILE  = __DIR__ . '/files/binaryfile';

	public function testCurrentIsCorrect() {
		$iterator = new FileIterator(self::TEXT_FILE);
		$iterator->next();

		self::assertEquals('1', $iterator->current());

		$iterator = new FileIterator(self::BIN_FILE);
		$iterator->next();

		self::assertEquals(1, ord($iterator->current()));
//		self::assertEquals(pack('C*', '1'), $iterator->current());
	}

	public function testWorksFineWithForeach() {
		$iterator = new FileIterator(self::TEXT_FILE);

		$str = '';
		foreach ($iterator as $c) {
			$str .= $c;
		}

		self::assertEquals('123ASDF', $str);

		return $iterator;
	}

	public function testSeek() {
		$iterator = new FileIterator(self::TEXT_FILE);
		$iterator->seek(4);

		self::assertEquals('S', $iterator->current());

		$iterator->seek(0);
		self::assertEquals('1', $iterator->current());
	}

	public function testRewind() {
		$iterator = $this->testWorksFineWithForeach();
		$iterator->rewind();

		self::assertEquals(1, $iterator->current());
	}

	public function testValid() {
		$iterator = new FileIterator(self::TEXT_FILE);
		$iterator->seek($iterator->getSize() + 10);

		self::assertFalse($iterator->valid());
	}
}