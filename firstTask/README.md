postLikes keeps track of user ratings of post. 
When user likes a post we insert new row where rating would be `1`
When user dislikes a post we insert or update new row where rating would be `-1`
If user removes his rating we just update rate to be `0`.
As we have unique composite index (`posts_id`,`users_id`) we can do upsert on
conflict using: 
```sql
INSERT INTO postLikes(users_id,posts_id,rate) VALUE (@uid,@pid,@rate)
ON duplicate KEY UPDATE
rate=@rate; 
``` 


Getting users based on post can be done using 
```sql
SELECT * FROM users WHERE id IN (
SELECT users_id FROM postLikedUsers WHERE `postId` = postId
);
```
